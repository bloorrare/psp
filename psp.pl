use Getopt::Std;
use Getopt::Long;
use Text::Table;

use lib './modules';
use Processes;

my %proc_info;
my @parameters = qw/pid user name state comm/;

getopts("A");
GetOptions(
	"parameters=s@" => \@parameters, 
);
my @processes = Processes::all_getproc;

foreach my $pid (@processes) {
	$proc_info{$pid} = Processes::getproc_stat $pid, @parameters;
}
# formatted output
my $table = Text::Table->new(
	@parameters;
);

