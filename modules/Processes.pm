package Processes; 

sub all_getproc{
	my @data;
	opendir PROCDIR, "/proc" || die "Can't open /proc: $!";
	foreach (readdir PROCDIR) {
		push @data, $1 if /^(\d+)$/;
	}
	@data;
}

# get PID and paramets
# return reference hash  
sub getproc_stat{
	my ($pid, $fomatted, %status) = (shift, sprintf("%s|"x$#_."%s", @_));
	open STATFILE, "<", "/proc/$pid/status" || warn "Can't open /proc/$pid/status: $!";
	while (<STATFILE>) {
		$status{lc $1}=$2 if (/^($fomatted):\s*(.*)/ig)
	}
	close STATFILE; 	
	\%status;
}

# get PID; return username which start
# process
sub getproc_username {
	readlink "/proc/1/exe"
}

# return allowed keys for processes
# which contains in status file
sub getproc_stat_keys {
	my @proc_keys;
	open STATFILE, "<", "/proc/1/status" || die "Can't open /proc/1/status: $!";
	while (<STATFILE>) {
		 push @proc_keys, $1 if (/^(.*):/)
	}
	close STATFILE;
	@proc_keys;
}

1;
