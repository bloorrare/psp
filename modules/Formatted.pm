package Formatted;
use Text::Table;

# get hash of arrays
sub form_output{
	my %proc_data = shift @_;
	my $table = Text::Table->new(keys %proc_data);
	$table->load(values %proc_data);
}
